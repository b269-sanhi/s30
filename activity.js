db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// total number of fruits on sale
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$name", total: {$sum: "$stock"}}}
  ]);


// total stock more than or equal to 20


// { $gte: [ <expression1>, <expression2> ] }


db.fruits.aggregate([

    {$group: {_id: "$name", avg_price: {$gte: [ "$stock", 20 ]}}},
    ])



db.fruits.aggregate(
   [
     {
       $project:
          {
            name: 1,
            stock: 1,
            qtyGte250: { $gte: [ "stock", 20 ] },
            _id: 0
          }
     }
   ]
)

 // Average operator

db.fruits.aggregate(
   [
    {$match: {onSale: true}},
     {
       $group:
         {
           _id: "$name",
           avg_price: { $avg: { $multiply: [ "$price", "$stock" ] } },
       
         }
     }
   ]

// max operator

db.fruits.aggregate([
    {$group: {_id: "$supplier_id", max_price: { $max: "$price" } }}
  ]);

